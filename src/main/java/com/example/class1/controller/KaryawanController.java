package com.example.class1.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.class1.model.KaryawanModel;
import com.example.class1.model.KotaModel;
import com.example.class1.service.KaryawanService;
import com.example.class1.service.KotaService;

@Controller
@RequestMapping("/karyawan")
public class KaryawanController {

	@Autowired
	private KaryawanService karyawanService;
	
	@Autowired
	private KotaService kotaService;
	
	@RequestMapping("/home")
	public String doHome() {
		return "/karyawan/home";
	}
	
	@RequestMapping("/tambah")
	public String doTambah(Model model) {
		this.doGenerateKode(model);
		this.doListKota(model);
		return "/karyawan/add";
	}
	
	@RequestMapping("/create")
	public String doCreate (@RequestParam("foto")MultipartFile foto, HttpServletRequest request) throws ParseException, IOException {
		
		String kodeKaryawan = request.getParameter("kodeKaryawan");
		String namaKaryawan = request.getParameter("namaKaryawan");
		String tglLahirKaryawan = request.getParameter("tglLahirKaryawan");
		Integer idKota = Integer.parseInt(request.getParameter("idKota"));
		
		KaryawanModel karyawanModel = new KaryawanModel();		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = format.parse(tglLahirKaryawan);
		
		karyawanModel.setKodeKaryawan(kodeKaryawan);
		karyawanModel.setNamaKaryawan(namaKaryawan);
		karyawanModel.setTglLahirKaryawan(date1);
		karyawanModel.setIsDelete(0);
		karyawanModel.setIdKota(idKota);	
		
		//setFileFoto
		karyawanModel.setNamaFoto(foto.getOriginalFilename());
		karyawanModel.setTipeFoto(foto.getContentType());
		karyawanModel.setFileFoto(foto.getBytes());
		
		this.karyawanService.create(karyawanModel);
		
		return "/karyawan/home";
	}
	
	public void doGenerateKode(Model model) {
		
		Integer maxId = 0;
		maxId = this.karyawanService.searchMaxId();
		if (maxId == null) {
			maxId = 1;
		} else {
			maxId += 1;
		}
		
		String kodeGenerator = "KARY00"+maxId;
		model.addAttribute("kodeGenerator", kodeGenerator);
	}
	
	@RequestMapping("/data")
	public String doList(Model model) {
		
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
		karyawanModelList = this.karyawanService.searchNotDelete();
		model.addAttribute("karyawanModelList", karyawanModelList);
		
		return "/karyawan/list";		
	}
	
	@RequestMapping("/hapus")
	public String doHapus (HttpServletRequest request) {
		String kodeKaryawan = request.getParameter("kodeKaryawan");
		
		KaryawanModel karyawanModel = new KaryawanModel();
		karyawanModel = this.karyawanService.searchKodeKaryawan(kodeKaryawan);
		
		karyawanModel.setIsDelete(1);
		this.karyawanService.update(karyawanModel);
		
		return "/karyawan/list";
		
	}	

	public void doListKota(Model model) {
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.read();
		model.addAttribute("kotaModelList", kotaModelList);		
	}
	
	@RequestMapping("/download")
	public ResponseEntity<Resource> doDownload(HttpServletRequest request) {         
        
		Integer idKaryawan = Integer.parseInt(request.getParameter("idKaryawan"));
		KaryawanModel karyawanModel = new KaryawanModel();
		karyawanModel = this.karyawanService.searchIdKaryawan(idKaryawan);
        return ResponseEntity.ok()             
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + karyawanModel.getNamaFoto()+"\"")
                .contentType(MediaType.parseMediaType(karyawanModel.getTipeFoto()))           
                .body(new ByteArrayResource(karyawanModel.getFileFoto()));
    }
	
	@RequestMapping("/show")
	public void doShow(HttpServletRequest req, HttpServletResponse res) throws IOException {
		Integer idKaryawan = Integer.parseInt(req.getParameter("idKaryawan"));
		KaryawanModel karyawanModel = new KaryawanModel();
		karyawanModel = this.karyawanService.searchIdKaryawan(idKaryawan);
		res.setContentType(karyawanModel.getTipeFoto());
		res.getOutputStream().write(karyawanModel.getFileFoto());
		res.getOutputStream().close();	
	
	}
}

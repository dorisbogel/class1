package com.example.class1.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.class1.model.AnggotaModel;
import com.example.class1.repository.AnggotaRepository;

@Service
@Transactional
public class AnggotaService {

	@Autowired
	private AnggotaRepository anggotaRepository;
	
	

	public void save(AnggotaModel anggotaModel) {
		anggotaRepository.save(anggotaModel);
		
	}
	
	public List<AnggotaModel> read() {
		return this.anggotaRepository.findAll(); 
	}

	

	public AnggotaModel searchKodeAnggota(String kodeAnggota) {
		
		return this.anggotaRepository.searchKodeAnggota(kodeAnggota);
	}

	public void update(AnggotaModel anggotaModel) {
		
		anggotaRepository.save(anggotaModel);
	}

	public void delete(AnggotaModel anggotaModel) {
		anggotaRepository.delete(anggotaModel);
	}
	
	public List<AnggotaModel> searchNamaAnggota(String namaAnggota){
		return this.anggotaRepository.searchNamaAnggota(namaAnggota);
		
	}
	//pattern dari service 
	// public output namaMethod(tipe input1, tipe inputN){}
	public void create(AnggotaModel anggotaModel) {
		
	}
	
	public void simpan(AnggotaModel anggotaModel) {
		
	}
	
	public void insert(AnggotaModel anggotaModel) {
		
	}
	public void mengubah(AnggotaModel anggotaModel) {
		
	}
	
	public AnggotaModel mencari(String namaAnggota) {
		return null;
	}
	public AnggotaModel cariinDonk(String kodeAnggota, String namaAnggota) {
		return null;
		
	}
	public List<AnggotaModel> cariKodeAnggotaDonk(String kodeAnggota) {
		return this.anggotaRepository.cariKodeAnggotaRepository(kodeAnggota);
		
	}
	
}

